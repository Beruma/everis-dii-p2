package dam2.dii.p2;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Operaciones {
	//Instancio un ArrayList donde guardare los contactos//
	static ArrayList<Contacto> listaContactos = new ArrayList<>();
	//Creo un metodo para recoger los datos introducidos por el usuario y si estan correctos agragarlos//
	public static void recogerDatos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Contacto contacto = new Contacto();
		String valor_nombre = request.getParameter("name");
		String valor_apellidos = request.getParameter("surname");
		String valor_correo = request.getParameter("mail");
		String valor_telefono = request.getParameter("phone");
		String valor_comentarios = request.getParameter("coments");
		String validaciones = "";
		
		if(!comprobarCampos(valor_nombre, valor_apellidos,valor_correo,valor_telefono)) {
			validaciones += "Por favor rellene todos los campos";
		}else if(!comprobarCorreo(valor_correo)) {
			validaciones += "El correo ya esta en uso";
		}else {
			contacto.setNombre(valor_nombre);
			contacto.setApellidos(valor_apellidos);
			contacto.setEmail(valor_correo);
			contacto.setTelefono(Integer.parseInt(valor_telefono));
			contacto.setComentarios(valor_comentarios);
			listaContactos.add(contacto);
		
			request.setAttribute("listaContactos", listaContactos);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		
		}
		request.setAttribute("frase", validaciones);
		request.getRequestDispatcher("/index.jsp").forward(request, response);
		
	}
	//metodo para comprobar qu los campos no se quedan vacios//
	public static boolean comprobarCampos(String nombre, String apellidos, String correo, String telefono) {
		if((nombre.equals(""))&&(apellidos.equals(""))&&(correo.equals(""))&&(telefono.equals(""))) {
			return false;
		}
		return true;
	}
	//metodo para comprobar que el correo no se repite en el Array//
	public static boolean comprobarCorreo(String correo) {
		for(Contacto c:listaContactos) {
			if(c.getEmail().equals(correo)) {
				return false;
			}
		}
		return true;
	}
	
}
