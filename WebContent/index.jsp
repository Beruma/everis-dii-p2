<%@page import="dam2.dii.p2.Contacto"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import='java.util.ArrayList' %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="CSS/style.css">
<meta charset="ISO-8859-1">
<title>Practica 2</title>
</head>
	<h1>Agenda</h1>
<body>
<div>
	<h4>Agregar contacto</h4>
	<div class="formulario">
	<form action="<%=request.getContextPath()%>/controlador" method="post"><!-- Los datos del formulario ser�n redireccionados a Operaciones.java por el m�todo doPost -->
		<% String frase = (String) request.getAttribute("frase"); 
		if(frase == null){
		frase = "";}%><!-- En eeste hueco aparecer� la frase seg�n si los datos son correctos o estan vacios -->
		<%=frase %>
		<br>
		Nombre:
		<br>
		<input type="text" name="name" placeholder="ejemplo: Pepe">
		<br>
		<br>
		Apellidos:
		<br>
		<input type="text" name="surname" placeholder="ejemplo: Perez Garcia">
		<br>
		<br>
		E-mail:
		<br>
		<input type="email" name="mail" placeholder="ejemplo: pepe@gmail.com">
		<br>
		<br>
		Tel�fono:
		<br>
		<input type="tel" name="phone" pattern="^(?:(?:\+|00)?34)?[6789]\d{8}$"><!-- pattern obligar� al usuario a meter un n�mero de tel�fono correcto -->
		<br>
		<br>
		Comentarios:
		<br>
		<input type="text" name="coments">
		<br>
		<input type="submit" class="enviarB" name="boton" value="Enviar">
		<br>
		
		<!-- ${requestScope.validaciones} --><!-- Si alg�n campo se queda en blanco o si el correo esta repetido aqu� saldr� la frase avisando al usuario -->
		<div>Lista de contactos:
		<br>
		<% ArrayList<Contacto> lista = (ArrayList<Contacto>) request.getAttribute("listaContactos");
			if(lista != null){
				for(Contacto c: lista){
					out.println("nombre: " + c.getNombre() + ", apellido: " + c.getApellidos() + ", email: " + c.getEmail() + ", telefono: " + c.getTelefono());
					%><br><% 
				}
			}
			
		%>
		<!--${requestScope.dataList}--></div><!-- Aqu� se mostrar� el ArrayList de contactos si los datos introducidos son correctos -->
	</form>
	</div>
</body>
</html>